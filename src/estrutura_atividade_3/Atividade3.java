package estrutura_atividade_3;

import java.util.ArrayDeque;
import java.util.ArrayList;

/**
 * 
 * @author Robson Mattos
 *
 */

public class Atividade3 {

	public static void main(String[] args) {
		Atividade3 atividade3 = new Atividade3();
		
		ArrayList<Integer> lista = new ArrayList<>(5);
		ArrayDeque<Integer> pilha = new ArrayDeque<>(5);
		ArrayDeque<Integer> fila = new ArrayDeque<>(10);
		
		atividade3.inserirnumerosLista(lista);							// passo 1
		atividade3.removerDadosDaListaEInsereNaPilha(lista, pilha);		// passo 2
		atividade3.removerDadosDaPilhaEInsereNaFila(pilha, fila);		// passo 3
		atividade3.removerDadosDaListaEInsereNaPilha(lista, pilha);		// passo 2
		atividade3.removerDadosDaPilhaEInsereNaFila(pilha, fila);		// passo 3
		
		System.out.println(fila);
	}	

	public void inserirnumerosLista(ArrayList<Integer> lista) {
		System.out.println("Inserindo os numeros 1,2,3,4,5 na lista");
		lista.add(1);
		lista.add(2);
		lista.add(3);
		lista.add(4);
		lista.add(5);
	}

	private void removerDadosDaListaEInsereNaPilha(ArrayList<Integer> lista, ArrayDeque<Integer> pilha) {
		System.out.println("--------------------------------------------");
		while (!lista.isEmpty()) {
//			System.out.println("Removendo " + lista.get(0));
			pilha.push(lista.get(0));
			lista.remove(0);
		}
		    
//		System.out.println(pilha);
	}
	
	private void removerDadosDaPilhaEInsereNaFila(ArrayDeque<Integer> pilha, ArrayDeque<Integer> fila) {
		System.out.println("--------------------------------------------");
		while (!pilha.isEmpty()) {
			System.out.println("Removendo " + pilha.getFirst());
			fila.add(pilha.pop());
		}
		
		fila.add(6);
		fila.add(7);
		fila.add(8);
		fila.add(9);
		fila.add(10);
		
//		System.out.println(fila);
	}
}
